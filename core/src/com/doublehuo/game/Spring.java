package com.doublehuo.game;

/**
 * Created by DoubleHUO on 7/4/18.
 */

public class Spring extends GameObject {
    public static float SPRING_WIDTH = 0.3f;
    public static float SPRING_HEIGHT = 0.3f;

    public Spring (float x, float y) {
        super(x, y, SPRING_WIDTH, SPRING_HEIGHT);
    }
}
